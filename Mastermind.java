package Mastermind;

import java.util.Scanner;

public class Mastermind {

	public static void main(String[] args) {
		char[][] tableroIntentos = new char[10][5];
		char[][] tableroIntentosCombrobacion = new char[10][5];
		char[] filaOculta = new char[5];
		
		rellenarFilaOculta(filaOculta);

		
		
		mostrarFilaOculta(filaOculta);
		
		System.out.println();
		
		
		
		mostrarHuecosPorFila(tableroIntentos);
		
		insertarFilaBolas(tableroIntentos, filaOculta);
		
		
		
	}

	





	private static void insertarFilaBolas(char[][] tableroIntentos, char[] filaOculta) {//metemos la bola en la fila y las contamos
		final int longitudVertical = 10;
		final int longitudHorizontal = 5;
		
		int nivelTablero=0;
		int contadorExitosParciales = 0;
		int contadorExitosCompletos = 0;
		
		final String fraseGanadora ="HAS GANADO!";
		final String fraseRepetitiva ="...INTÉNTALO DE NUEVO...";
		
		for(int i = 0; i<longitudVertical; i++) {
			for(int j = 0; j<longitudHorizontal; j++) {
				
				
				
				tableroIntentos[i][j]=insertarColorFila();
		
//				nivelTablero++;
				
//				contadorExitosParciales = comprobarIntentosParcialesTablero(tableroIntentos, filaOculta, nivelTablero);	
//				contadorExitosCompletos = comprobarIntentosCompletosTablero(tableroIntentos, filaOculta, nivelTablero);
				
				
			}
			
			contadorExitosParciales = comprobarIntentosParcialesTablero(tableroIntentos, filaOculta, nivelTablero);	
			contadorExitosCompletos = comprobarIntentosCompletosTablero(tableroIntentos, filaOculta, nivelTablero);
			
			
			mostrarBolasInsertadas(longitudVertical, longitudHorizontal, tableroIntentos);
			
			boolean resultadoContador = comprobarSiJugadorGana(contadorExitosParciales, contadorExitosCompletos);
			
			mostrarInicialesCorrectas(tableroIntentos, filaOculta, nivelTablero);
			
			mostrarFinalDeLaPartida(resultadoContador, fraseRepetitiva, fraseGanadora);
			
			contadorExitosParciales=0;
			contadorExitosCompletos=0;
			
			nivelTablero++;
		}
		
	}





	private static void mostrarInicialesCorrectas(char[][] tableroIntentos, char[] filaOculta, int nivelTablero) {
		
		char[] inicial = new char[5];
		
		for(int i = 0; i<filaOculta.length; i++) {
			if(tableroIntentos[nivelTablero][i]==filaOculta[i]) {
				inicial[i]=tableroIntentos[nivelTablero][i];
			}
		}
		
		System.out.println("Las iniciales: ");
		
		for(int j = 0; j<inicial.length; j++) {
			System.out.print("\t"+inicial[j]+" ");
		}
		System.out.println("Son correctas");
		System.out.println();
	}







	private static void mostrarFinalDeLaPartida(boolean resultadoContador, String fraseRepetitiva, String fraseGanadora) {
		if(resultadoContador==true) {
			System.out.println(fraseGanadora);
		}else {
			System.out.println(fraseRepetitiva);
			System.out.println();
		}
		
	}







	private static boolean comprobarSiJugadorGana(int contadorExitosParciales, int contadorExitosCompletos) {
		
		boolean victoria = false;
		
		
		if(contadorExitosParciales<=5) {
			
			
			victoria=false;
			
			
		}else if(contadorExitosCompletos==5) {
			
			victoria=true;
			
		}
		
		System.out.println("Tu contador de exitos parciales es "+contadorExitosParciales);
		System.out.println("Tu contador de exitos es "+contadorExitosCompletos);
		
		return victoria;
	}







	private static void mostrarBolasInsertadas(int longitudVertical, int longitudHorizontal, char[][] tableroIntentos) {
		for(int y = 0; y<longitudVertical; y++) {
			for(int x = 0; x<longitudHorizontal; x++) {
				System.out.print("\t"+tableroIntentos[y][x]);
			}	
			System.out.println();
		}
		
	}







	private static int comprobarIntentosCompletosTablero(char[][] tableroIntentos, char[] filaOculta, int nivelTablero) {
		
		int contadorCompleto = 0;
		
		for(int i = 0; i<filaOculta.length; i++) {
			if(tableroIntentos[nivelTablero][i]==filaOculta[i]) {
				contadorCompleto++;
			}
		}
		return contadorCompleto;
	}







	private static int comprobarIntentosParcialesTablero(char[][] tableroIntentos, char[] filaOculta, int nivelTablero) {
		
		final char BLANCO = 'B';
		final char NEGRO = 'N';
		final char AMARILLO = 'A';
		final char VERDE = 'V';
		final char ROJO = 'R';
		final char ORO = 'O';
		final char PLATA = 'P';
		
		final int filaIntentosTablero = 5;
		
		int contadorParcial = 0;
		
		for(int i = 0; i<filaIntentosTablero; i++) {
			for(int j = 0; j<filaOculta.length; j++) {
				
				System.out.println("El contenido del tablero es "+tableroIntentos[nivelTablero][i]);
				System.out.println("El contenido de fila oculta es "+filaOculta[j]);
				if(tableroIntentos[nivelTablero][i]==filaOculta[j]){
					contadorParcial++;	
					break;
				}
				if(tableroIntentos[nivelTablero][i]!=filaOculta[i]) {
					continue;
				}
				
			}
		}
		
		
		return contadorParcial;
	}







//	private static char comprobarIntentosParcialesFIlaOculta(char[] filaOculta) {
//		for(int i = 0; i<filaOculta.length; i++) {
//			
//		}
//		
//		return 0;
//	}







	private static char insertarColorFila() {//Introducimos el color
		Scanner sc = new Scanner(System.in);
		System.out.print("Escriba la inicial de un color: ");
		char bola = sc.next().charAt(0);
		
		return bola;
	}





	private static void mostrarHuecosPorFila(char[][] tableroIntentos) {//muestra el tblero vacío
		final int longitudVertical = 10;
		final int longitudHorizontal = 5;
		
		for(int i = 0; i<longitudVertical; i++) {
			for(int j = 0; j<longitudHorizontal; j++) {
				tableroIntentos[i][j]='_';
			}
		}
		
		for(int i = 0; i<longitudVertical; i++) {
			for(int j = 0; j<longitudHorizontal; j++) {
				System.out.print("\t"+tableroIntentos[i][j]);
			}
			
			System.out.println();
			
		}
		
	}





	private static char obtenerColor() {
		Scanner sc = new Scanner(System.in);
		System.out.println();
		System.out.println("Introduzca un color: ");
		char letraInicialColor = sc.next().charAt(0);
		return letraInicialColor;
		
	}





	private static void rellenarFilaOculta(char[] filaOculta) {
		for(int i = 0; i<filaOculta.length; i++) {
			char color = generarColorCodigo();
			filaOculta[i]=color;
		}
		
	}

	private static void mostrarFilaOculta(char[] filaOculta) {
		for(int i =  0; i<filaOculta.length; i++) {
			System.out.print("\t"+filaOculta[i]);
		}
		
	}





	private static char generarColorCodigo() {
		final int NUM_COLORES = 7;
		
		int POS_COLOR = (int) (Math.random()* NUM_COLORES+ 1);
		
		char color = asignarCodigoAColor(POS_COLOR);
		
		return color;
		
	}





	private static char asignarCodigoAColor(int POS_COLOR) {
		char color=' ';
		
		final char BLANCO = 'B';
		final char NEGRO = 'N';
		final char AMARILLO = 'A';
		final char VERDE = 'V';
		final char ROJO = 'R';
		final char ORO = 'O';
		final char PLATA = 'P';
		
		if(POS_COLOR==1) {
			color=BLANCO;
		}else if(POS_COLOR==2){
			color=NEGRO;
		}else if (POS_COLOR==3) {
			color=AMARILLO;
		}else if (POS_COLOR==4) {
			color=VERDE;
		}else if (POS_COLOR==5) {
			color=ROJO;
		}else if (POS_COLOR==6) {
			color=ORO;
		}else if (POS_COLOR==7) {
			color=PLATA;
		}
		
		return color;
	}
	
	




	

}
